import traceback
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions 
 

lastDab_URL = "https://heatonist.com/collections/brands/products/hot-ones-the-last-dab?variant=378416136201"
other_URL = "https://heatonist.com/collections/brands/products/secret-aardvark-habanero-hot-sauce?variant=378224443401"

xpath_to_lastDab_home = '//*[@id="shopify-section-featured-products"]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/a[1]/div[1]/img[1]'

driver = webdriver.Chrome()
driver.get("https://heatonist.com/products/hot-ones-the-last-dab?variant=378416136201")

isAddedToCart = False
seenAddressPage = False
seenShippingPage = False
seenPassedPayLogin = False

def loginToPayPal():
    global seenPassedPayLogin
    if(not seenPassedPayLogin):
        WebDriverWait(driver, 50).until(expected_conditions.presence_of_element_located((By.XPATH, '//*[@id="password"]'))).send_keys("balhhh")
        driver.find_element_by_xpath('//*[@id="btnLogin"]').send_keys(Keys.ENTER)

    seenPassedPayLogin = True
    
    WebDriverWait(driver, 50).until(expected_conditions.presence_of_element_located((By.XPATH, '//*[@id="paymentMethod"]/a[1]'))).send_keys(Keys.PAGE_DOWN)
    WebDriverWait(driver, 50).until(expected_conditions.presence_of_element_located((By.XPATH, '//*[@id="confirmButtonTop"]'))).click()




def purchaseItem():
    global isAddedToCart, seenAddressPage, seenShippingPage
    #if we cant checkout we will refresh the page
    try:
        if(not isAddedToCart):
            item = WebDriverWait(driver, 10).until(expected_conditions.presence_of_element_located((By.ID, "AddToCartText")))
            item.click()

            WebDriverWait(driver, 3).until(expected_conditions.presence_of_element_located((By.XPATH, '//*[@id="CartSpecialInstructions"]')))

            WebDriverWait(driver, 10).until(expected_conditions.presence_of_element_located((By.XPATH, '//*[@id="CartSpecialInstructions"]'))).send_keys(Keys.PAGE_DOWN)
            
            #the side panel has loaded
            cart = WebDriverWait(driver, 10).until(expected_conditions.presence_of_element_located((By.XPATH, '//*[@id="CartContainer"]/form[1]/div[3]/button[1]')))
            cart.send_keys(Keys.PAGE_DOWN)
            cart.click()
            
            isAddedToCart = True
        else:
            WebDriverWait(driver, 3).until(expected_conditions.presence_of_element_located((By.XPATH, '//*[@id="shopify-section-header"]/div[1]/header[1]/div[1]/div[1]/div[3]/ul[1]/li[4]/a[1]/i[1]'))).click()

        #wait for the checkout page to load
        WebDriverWait(driver, 10).until(expected_conditions.presence_of_element_located((By.XPATH, '//*[@id="site-title"]/a[1]/img[1]')))

        #we will actaully initiate the purchase
        item =     WebDriverWait(driver, 10).until(expected_conditions.presence_of_element_located((By.XPATH, '//*[@id="shopify-section-cart-template"]/form[1]/div[1]/div[2]/input[2]')))
        item.send_keys(Keys.PAGE_DOWN)
        item.click()

        if(not seenAddressPage):
            #address page
            WebDriverWait(driver, 10).until(expected_conditions.presence_of_element_located((By.XPATH, '//*[@id="checkout_reduction_code"]'))).send_keys(Keys.PAGE_DOWN)
            WebDriverWait(driver, 10).until(expected_conditions.presence_of_element_located((By.CSS_SELECTOR, 'html > body > div:nth-of-type(2) > div > div > div:nth-of-type(2) > div > form > div:nth-of-type(2) > button > svg'))).click()
        
        if(not seenShippingPage):
            #shipping page
            WebDriverWait(driver, 10).until(expected_conditions.presence_of_element_located((By.CSS_SELECTOR, 'html > body > div:nth-of-type(2) > div > div > div:nth-of-type(2) > div > form > div:nth-of-type(2) > button'))).click()
            seenAddressPage = True

        #payment method page
        wait = WebDriverWait(driver, 10).until(expected_conditions.presence_of_element_located((By.ID, "checkout_payment_gateway_120095369")))
        
        seenShippingPage = True

        wait.click()
        wait.send_keys(Keys.PAGE_DOWN)
        wait.send_keys(Keys.ENTER)

        #Clicking the Complete order button
        #WebDriverWait(driver, 10).until(expected_conditions.presence_of_element_located((By.NAME, "Complete order"))).click()

        loginToPayPal()
    except Exception as e:
        print(''.join(traceback.format_exception(etype=type(e), value=e, tb=e.__traceback__)))
        return False

    return True

def goToHomepage():
    gotoHome = driver.find_element_by_xpath('//*[@id="site-title"]/a[1]/img[1]')
    gotoHome.click()

def loginToAccount(username, password):
    wait_for_email = WebDriverWait(driver, 10).until(expected_conditions.presence_of_element_located((By.ID, "CustomerEmail")))

    #email_text = driver.find_element_by_id("CustomerEmail")
    password_text = driver.find_element_by_id("CustomerPassword")
    signIn_button = driver.find_element_by_id("customer_login")

    wait_for_email.send_keys(username)
    password_text.send_keys(password)
    signIn_button.click()



def main(item_URL):
    account_button = driver.find_element_by_xpath('//*[@id="shopify-section-header"]/div[1]/header[1]/div[1]/div[1]/div[3]/ul[1]/li[3]/a[1]/i[1]')
    account_button.click()
    loginToAccount("blahh@gmail.com", "blahh")
    driver.get(item_URL)

    while not purchaseItem():
        driver.get(item_URL)
        print("trying again...")



#elem1.click()
#elem.send_keys(Keys.RETURN)
#assert "No results found." not in driver.page_source
#driver.close()

main(lastDab_URL)
#driver.close()